<%-- 
    Document   : buscarPatrimonio
    Created on : 27-Jun-2017, 10:01:50 AM
    Author     : Walter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Busca de Patrimônio</title>
    </head>
    <body>
        <form action="/sca/patrimonio/manterPatrimonio/realizarBusca" method="get">
            Número: <input type="text" name="numero" ><br>
            Descrição: <input type="text" name="desc"><br>
            Adquirido entre: <input type="date" name="adqIn"> e <input type="date" name="adqFn"><br>
            Com baixa entre: <input type="date" name="bxIn"> e <input type="date" name="bxFn"><br>
            Encarregado: <input type="text" name="encarregado"><br>
            <input type="submit">
        </form>
    </body>
</html>
