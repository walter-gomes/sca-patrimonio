<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de itens de patrimônio</title>
    </head>
    <body>
        <fieldset>
            <table>
                <tr>
                    <th>Número</th>
                    <th>Descrição</th>
                    <th>Encarregado</th>
                    <th>Data de Inserção</th>
                    <th>Última data de baixa</th>
                    <th>Editar Item</th>
                </tr>
                <c:forEach var="item" items="${listaPatrimonio}">
                    <tr>
                        <td>${item.getNumero()}</td>
                        <td>${item.getDescricao()}</td>
                        <c:choose>
                            <c:when test="${item.getUltimaRetirada().getEncarregado().getProfessor().getPessoa().getNome() != null}">
                                <td>${item.getUltimaRetirada().getEncarregado().getProfessor().getPessoa().getNome()}</td>
                            </c:when>
                            <c:otherwise>
                                <td>Nenhum</td>
                            </c:otherwise>
                        </c:choose>
                        <td>${item.getData_aquisicao()}</td>
                        <c:choose>
                            <c:when test="${item.getData_baixa() != null}">
                                <td>${item.getData_baixa()}</td>
                            </c:when>
                            <c:otherwise>
                                <td>Nunca</td>
                            </c:otherwise>
                        </c:choose>
                        <td>
                            <form action="/sca/patrimonio/manterPatrimonio/editarPatrimonio" method="post">
                                <input type="hidden" name="numero" value=${item.getNumero()}>
                                <input type="submit" value="Editar"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </fieldset>
        <fieldset>
            <legend>Adicionar um novo item</legend>
            <form action="/sca/patrimonio/manterPatrimonio/adicionarPatrimonio" method="post">
                Número: <input type="text" name="numero"><br>
                Descrição: <input type="text" name="descricao"><br>
                Data de Aquisição: <input type="date" name="data_aquisicao"><br>
                <input type="submit">
            </form>
        </fieldset>
    </body>
</html>
