<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edição de Item de Patrimônio</title>
    </head>
    <body>
        <fieldset>
            <legend>Editar item</legend>
            <form action="/sca/patrimonio/manterPatrimonio/atualizarPatrimonio" method="post">
                Número: <input type="text" name="numero" value=${item.getNumero()}><br>
                Descrição: <input type="text" name="descricao" value="${item.getDescricao()}"><br>
                Data de Aquisição: <input type="date" name="data_aquisicao" value=${item.getData_aquisicao()}><br>
                Data de Baixa: <input type="date" name="data_baixa" value=${item.getData_baixa()}><br>
                Ultimo Encarregado: ${encarregado.getProfessor().getPessoa().getNome()}<br>
                Encarregado: <select name="cargoId">
                    <option value="0">Nenhum</option>
                    <c:forEach var="cargo" items="${encarregadosDisponiveis}">
                        <option value="${cargo.getId()}">${cargo.getProfessor().getPessoa().getNome()}</option>
                    </c:forEach>
                </select>
                <input type="submit" name="Salvar">
            </form>
            <br>
            <br>
            <br>
            <form action="/sca/patrimonio/manterPatrimonio/deletarPatrimonio" method="post">
                <input type="hidden" name="id" value=${item.getId()}>
                <input type="submit" value="DELETAR">
            </form>
        </fieldset>
    </body>
</html>
