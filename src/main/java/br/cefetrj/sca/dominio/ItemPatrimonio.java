/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.dominio;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class ItemPatrimonio {
    
    @GeneratedValue
    @Id
    Long id;
    
    Long numero;
    
    String descricao;
    
    @Temporal(TemporalType.TIMESTAMP)
    Date data_aquisicao;

    @Temporal(TemporalType.TIMESTAMP)
    Date data_baixa;
    
    @OneToMany(mappedBy="item")
    List<Retirada> retiradas;
    
    @OneToOne
    Retirada ultimaRetirada;
    
    @SuppressWarnings("unused")
    public ItemPatrimonio(){
    }
    
    public ItemPatrimonio(Long numero, String descricao, Date data_aquisicao, Date data_baixa){
        this.numero = numero;
        this.descricao = descricao;
        this.data_aquisicao = data_aquisicao;
        this.data_baixa = data_baixa;
    }
    
    public Long getId() {
        return id;
    }

    public Long getNumero() {
        return numero;
    }

    public String getDescricao() {
        return descricao;
    }

    public Date getData_aquisicao() {
        return data_aquisicao;
    }

    public Date getData_baixa() {
        return data_baixa;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setData_aquisicao(Date data_aquisicao) {
        this.data_aquisicao = data_aquisicao;
    }

    public void setData_baixa(Date data_baixa) {
        this.data_baixa = data_baixa;
    }

    public List<Retirada> getRetiradas() {
        return retiradas;
    }

    public void setRetiradas(List<Retirada> retiradas) {
        this.retiradas = retiradas;
    }

    public Retirada getUltimaRetirada() {
        return ultimaRetirada;
    }

    public void setUltimaRetirada(Retirada ultimaRetirada) {
        this.ultimaRetirada = ultimaRetirada;
    }
    
    
}
