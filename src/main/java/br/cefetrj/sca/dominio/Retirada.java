/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.dominio;

import java.util.Comparator;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Walter
 */
@Entity
public class Retirada{
    
    @Id
    @GeneratedValue
    Long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    Date dataEntrada;
    
    @Temporal(TemporalType.TIMESTAMP)
    Date dataSaida;
    
    @ManyToOne
    @JoinColumn(name = "CARGO_ID")
    Cargo encarregado;
    
    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    ItemPatrimonio item;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(Date dataSaida) {
        this.dataSaida = dataSaida;
    }

    public Cargo getEncarregado() {
        return encarregado;
    }

    public void setEncarregado(Cargo encarregado) {
        this.encarregado = encarregado;
    }

    public ItemPatrimonio getItem() {
        return item;
    }

    public void setItem(ItemPatrimonio item) {
        this.item = item;
    }
    
}
