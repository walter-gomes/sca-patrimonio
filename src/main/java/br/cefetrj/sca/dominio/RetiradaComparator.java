/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.dominio;

import java.util.Comparator;

/**
 *
 * @author Walter
 */
public class RetiradaComparator implements Comparator<Retirada>{
        
    @Override
    public int compare(Retirada a, Retirada b){
        if (a.getDataSaida() == null){
            return -1;
        }
        else if (b.getDataSaida() == null){
            return 1;
        }
        else if (a.getDataEntrada().before(b.getDataEntrada())){
            return 1;
        }
        else if(a.getDataEntrada().after(b.getDataEntrada())){
            return -1;
        }
        else{
            return 0;
        }
    }
    
}
