/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.dominio.repositories;

import br.cefetrj.sca.dominio.ItemPatrimonio;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ItemPatrimonioRepositorio extends
		JpaRepository<ItemPatrimonio, Serializable> {
    
   public ItemPatrimonio findByNumero(Long numero);
   
   public List<ItemPatrimonio> findByDescricao(String descricao);
   
   @Query("SELECT i FROM ItemPatrimonio i WHERE i.data_aquisicao >= ?1 AND i.data_aquisicao <= ?2")
   public List<ItemPatrimonio> findByAquisicaoBetweenDates(Date data_inicio, Date data_termino);
   
   @Query("SELECT i FROM ItemPatrimonio i WHERE i.data_baixa >= ?1 AND i.data_aquisicao <= ?2")
   public List<ItemPatrimonio> findByBaixaBetweenDates(Date data_inicio, Date data_termino);
   
   @Query("SELECT r.item FROM Retirada r WHERE r.encarregado.professor.pessoa.nome = ?1")
   public List<ItemPatrimonio> findByEncarregado(String nome);
}
