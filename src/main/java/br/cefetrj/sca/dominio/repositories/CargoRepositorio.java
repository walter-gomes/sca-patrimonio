/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.dominio.repositories;

import br.cefetrj.sca.dominio.Cargo;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Walter
 */
public interface CargoRepositorio extends JpaRepository<Cargo, Serializable>{
    
}
