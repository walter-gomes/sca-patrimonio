/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.service;

import br.cefetrj.sca.dominio.Cargo;
import br.cefetrj.sca.dominio.ItemPatrimonio;
import br.cefetrj.sca.dominio.Retirada;
import br.cefetrj.sca.dominio.repositories.CargoRepositorio;
import br.cefetrj.sca.dominio.repositories.ItemPatrimonioRepositorio;
import br.cefetrj.sca.dominio.repositories.RetiradaRepositorio;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

@Component
public class ManterPatrimonioService {
    
    @Autowired
    private ItemPatrimonioRepositorio repo;
    
    @Autowired
    private CargoRepositorio cargoRepo;
    
    @Autowired
    private RetiradaRepositorio retiRepo;
    
    public ItemPatrimonio findItemPatrimonioByNumero(Long numero){
        return repo.findByNumero(numero);
    }
    
    public ItemPatrimonio findItemPatrimonioById(Long id){
        return repo.findOne(id);
    } 
    
     public List<ItemPatrimonio> findItemPatrimonioByDescricao(String desc){
        return repo.findByDescricao(desc);
     }
    
     public List<ItemPatrimonio> findItemPatrimonioByAquisicaoBetween(Date a, Date b){
         return repo.findByAquisicaoBetweenDates(a, b);
     }
     
     public List<ItemPatrimonio> findItemPatrimonioByBaixaBetween(Date a, Date b){
         return repo.findByBaixaBetweenDates(a, b);
     }
     
     public List<ItemPatrimonio> findItemPatrimonioByEncarregado(String nome){
         return repo.findByEncarregado(nome);
     }
     
    public List<Cargo> findAllCargo(){
        return cargoRepo.findAll();
    }
    
    public Cargo findCargoById(Long id){
        return cargoRepo.findOne(id);
    }
    
    public List<Cargo> findEncarregadosById(Long id){
        ItemPatrimonio i = repo.findOne(id);
        List<Retirada> retiradas = i.getRetiradas();
        List<Cargo> encarregados = new ArrayList<Cargo>();
        for(Retirada retirada : retiradas){
            encarregados.add(retirada.getEncarregado());
        }
        return encarregados;
    }
    
    public List<ItemPatrimonio> findAll(){
        return repo.findAll();
    }
    
    public boolean salvarItemPatrimonio(ItemPatrimonio i){
        try{
            repo.save(i);
            return true;
        }
        catch(Exception e){
            System.out.println("FALHA AO SALVAR ITEM PATRIMONIO");
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean salvarItemPatrimonio(Long numero, String descricao, Date data_aquisicao, Date data_baixa){
        try{
            ItemPatrimonio i = new ItemPatrimonio(numero, descricao, data_aquisicao, data_baixa);
            repo.save(i);
            return true;
        }
        catch(Exception e){
            System.out.println("FALHA AO SALVAR ITEM PATRIMONIO");
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean deletarItemPatrimonio(ItemPatrimonio i){
        try{
            repo.delete(i);
            return true;
        }
        catch(Exception e){
            System.out.println("FALHA AO DELETAR ITEM PATRIMONIO");
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean salvarCargo(Cargo c){
        try{
            cargoRepo.save(c);
            return true;
        }
        catch(Exception e){
            System.out.println("FALHA AO SALVAR CARGO");
            e.printStackTrace();
            return false;
        }
    }
    
     public boolean salvarRetirada(Retirada r){
        try{
            retiRepo.save(r);
            return true;
        }
        catch(Exception e){
            System.out.println("FALHA AO SALVAR CARGO");
            e.printStackTrace();
            return false;
        }
    }
}
