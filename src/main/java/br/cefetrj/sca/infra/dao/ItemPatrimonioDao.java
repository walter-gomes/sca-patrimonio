/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.infra.dao;

import br.cefetrj.sca.dominio.ItemPatrimonio;
import java.util.List;

public interface ItemPatrimonioDao {
    
    public List<ItemPatrimonio> findAll();
    
    public ItemPatrimonio create(ItemPatrimonio i);
    
    public ItemPatrimonio findItemByNumero(Long numero);
    
}
