/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.infra.dao;

import br.cefetrj.sca.dominio.ItemPatrimonio;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class JpaItemPatrimonioDaoImpl implements ItemPatrimonioDao{
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public List<ItemPatrimonio> findAll() {
        return em.createQuery("SELECT * FROM ItemPatrimonio").getResultList();
    }

    @Override
    public ItemPatrimonio create(ItemPatrimonio i) {
        if (i.getId() <= 0){
            em.persist(i);
        }
        else{
            i = em.merge(i);
        }
        return i;
    }

    @Override
    public ItemPatrimonio findItemByNumero(Long numero) {
        TypedQuery<ItemPatrimonio> query = em.createQuery("SELECT i FROM ItemPatrimonio WHERE i.numero = ?1", ItemPatrimonio.class);
        query.setParameter(1, numero);
        try{
            return query.getSingleResult();
        }
        catch(NonUniqueResultException | NoResultException e){
            return null;
        }
    }
    
}
