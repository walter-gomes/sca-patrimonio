/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.cefetrj.sca.web.controllers;

import br.cefetrj.sca.dominio.Cargo;
import br.cefetrj.sca.dominio.ItemPatrimonio;
import br.cefetrj.sca.dominio.Retirada;
import br.cefetrj.sca.dominio.RetiradaComparator;
import br.cefetrj.sca.service.ManterPatrimonioService;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/patrimonio")
public class ManterItensPatromonioController {
    
    @Autowired
    private ManterPatrimonioService service;
    
    @RequestMapping(value = "/{*}", method = RequestMethod.GET)
    public String get(Model model) {
            model.addAttribute("error", "Erro: página não encontrada.");
            return "/homeView";
    }
    
    @RequestMapping(value = "/manterPatrimonio", method = RequestMethod.GET)
    public String manterPatrimonio(HttpServletRequest req, Model model){
        try {
            List<ItemPatrimonio> listaPatrimonio = service.findAll();
            model.addAttribute("listaPatrimonio", listaPatrimonio);
            List<Cargo> encarregadosDisponiveis = service.findAllCargo();
            model.addAttribute("listaCargo", encarregadosDisponiveis);
            return "/patrimonio/manterPatrimonioView";
        } catch (Exception exc) {
            model.addAttribute("error", exc.getMessage());
            return "homeView";
        }
    }
    
    @RequestMapping(value = "/manterPatrimonio/adicionarPatrimonio", method = RequestMethod.POST)
    public String adicionarPatrimonio(HttpServletRequest req, Model model) throws ParseException{
        try{
            Long numero = Long.parseLong(req.getParameter("numero"));
            String descricao = req.getParameter("descricao");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date data_aquisicao = df.parse(req.getParameter("data_aquisicao"));
            if(data_aquisicao == null) data_aquisicao = new Date();
            ItemPatrimonio i = new ItemPatrimonio(numero,descricao,data_aquisicao,null);
            service.salvarItemPatrimonio(i);
            return manterPatrimonio(req,model);
        }
        catch(NumberFormatException e){
            model.addAttribute("error", e.getMessage());
            return "homeView";
        }
    }
    
    @RequestMapping(value = "/manterPatrimonio/editarPatrimonio", method = RequestMethod.POST)
    public String editarPatrimonio(HttpServletRequest req, Model model){
        try{
            Long numero = Long.parseLong(req.getParameter("numero"));
            ItemPatrimonio i = service.findItemPatrimonioByNumero(numero);
            List<Cargo> disponiveis = service.findAllCargo();
            if (i.getUltimaRetirada() != null){
                Retirada ultimaRetirada = i.getUltimaRetirada();
                Cargo c = ultimaRetirada.getEncarregado();
                //model.addAttribute("retirada", ultimaRetirada);
                model.addAttribute("encarregado", c);
            }
            else{
                //model.addAttribute("retirada", null);
                model.addAttribute("encarregado", null);
            }
            model.addAttribute("item", i);
            model.addAttribute("encarregadosDisponiveis", disponiveis);
            return "/patrimonio/editarPatrimonioView";
        }
        catch(Exception e){
            model.addAttribute("error", e.getMessage());
            return "homeView";
        }
    }
    
    @RequestMapping(value = "/manterPatrimonio/atualizarPatrimonio", method = RequestMethod.POST)
    public String atualizarPatrimonio(HttpServletRequest req, Model model){
        try{
            Long numero = Long.parseLong(req.getParameter("numero"));
            ItemPatrimonio i = service.findItemPatrimonioByNumero(numero);
            Long idCargo = Long.parseLong(req.getParameter("cargoId"));
            Cargo encarregado = service.findCargoById(idCargo);
            String descricao = req.getParameter("descricao");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date data_aquisicao = new Date();
            Date data_baixa = new Date();
            try{
                data_aquisicao = df.parse(req.getParameter("data_aquisicao"));
            }
            catch(Exception e){
                System.out.println("O usuário fez besteira. Data_aquisicao é unparseable.");
            }
            try{
                data_baixa = df.parse(req.getParameter("data_baixa"));
            }
            catch(Exception e){
                System.out.println("O usuário fez besteira. Data_baixa é unparseable.");
            }
            Retirada retirada = null;
            Cargo anterior = new Cargo();
            if (i.getUltimaRetirada() != null){
                retirada = i.getUltimaRetirada();
                anterior = retirada.getEncarregado();
            }
            Retirada novaRetirada = new Retirada();
            if(anterior != null){
                if (idCargo != 0 && !Objects.equals(idCargo, anterior.getId())){
                    if(retirada.getDataSaida() == null){
                        retirada.setDataSaida(new Date());
                    }
                    novaRetirada.setEncarregado(encarregado);
                }
                else{
                    novaRetirada.setEncarregado(anterior);
                }
            }
            else novaRetirada.setEncarregado(encarregado);
            novaRetirada.setDataEntrada(new Date());
            novaRetirada.setItem(i);
            i.setNumero(numero);
            i.setDescricao(descricao);
            i.setData_aquisicao(data_aquisicao);
            i.setData_baixa(data_baixa);      
            service.salvarRetirada(novaRetirada);
            i.getRetiradas().add(novaRetirada);
            i.setUltimaRetirada(novaRetirada);
            service.salvarItemPatrimonio(i);
            return manterPatrimonio(req,model);
        }
        catch(Exception e){
            model.addAttribute("error", e.getMessage());
            return "homeView";
        }
    }
    
    @RequestMapping(value = "/manterPatrimonio/deletarPatrimonio", method = RequestMethod.POST)
    public String deletarPatrimonio(HttpServletRequest req, Model model){
        try{
            Long id = Long.parseLong(req.getParameter("id"));
            ItemPatrimonio i = service.findItemPatrimonioById(id);
            service.deletarItemPatrimonio(i);
            return manterPatrimonio(req,model);
        }
        catch(Exception e){
            model.addAttribute("error", e.getMessage());
            return "homeView";
        }
    }
    
    @RequestMapping(value = "/manterPatrimonio/buscarPatrimonio", method = RequestMethod.GET)
    public String buscarPatrimonio(HttpServletRequest req, Model model){
        return "/patrimonio/buscarPatrimonioView";
    }
    
    @RequestMapping(value = "/manterPatrimonio/realizarBusca", method = RequestMethod.GET)
    public String realizarBusca(HttpServletRequest req, Model model){
        // Iniciamos parseando todos os elementos do form
        // Não precisamos nos preocupar com elementos nulos ainda
        Long numero;
        if(req.getParameter("numero").equals("")){
            numero = null;
        }
        else{
            numero = Long.parseLong(req.getParameter("numero"));
        }
        String desc;
        if(req.getParameter("desc").equals("")){
            desc = null;
        }
        else{
            desc = req.getParameter("desc");
        }
        String encarregado;
        if(req.getParameter("encarregado").equals("")){
            encarregado = null;
        }
        else{
            encarregado = req.getParameter("encarregado");
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date data_aquisicao_inicio = new Date();
        Date data_aquisicao_termino = new Date();       
        Date data_baixa_inicio = new Date();
        Date data_baixa_termino = new Date();
        // Usamos estes booleans para registrar o que foi utilizado
        Boolean usarDateBx = false;
        Boolean usarDateAq = false;
        Boolean usarDesc = false;
        Boolean usarEncarregado = false;
        // Estas listas vão ser populadas com os resultados das queries
        List<ItemPatrimonio> listaPatrimonio = new ArrayList<>();
        List<ItemPatrimonio> porDesc = new ArrayList<>();
        List<ItemPatrimonio> porAquisicao = new ArrayList<>();
        List<ItemPatrimonio> porBaixa = new ArrayList<>();
        List<ItemPatrimonio> porEncarregado = new ArrayList<>();
        // Tentamos parsear as datas
        // Caso não dê certo (ParseException), não as utilizaremos
        try{
            data_aquisicao_inicio = df.parse(req.getParameter("adqIn"));
            data_aquisicao_termino = df.parse(req.getParameter("adqFn"));
            usarDateAq = true;
        }
        catch(ParseException e){
        }
        try{
            data_baixa_inicio = df.parse(req.getParameter("bxIn"));
            data_baixa_termino = df.parse(req.getParameter("bxFn"));
            usarDateBx = true;
        }
        catch(ParseException e){
        }
        // Agora começamos as queries
        // Caso os nossos parâmetros não sejam null, podemos fazer a query
        if(numero != null){
            ItemPatrimonio porNumero = service.findItemPatrimonioByNumero(numero);
            listaPatrimonio.add(porNumero);
            // Como numero é PK, só irá retornar um item
            // Já podemos o retornar direto
            model.addAttribute("listaPatrimonio", listaPatrimonio);
            return "/patrimonio/manterPatrimonioView";
        }
        // Busca por descrição
        if(desc != null){
            usarDesc = true;
            // Populamos as duas listas para depois filtrar
            porDesc = service.findItemPatrimonioByDescricao(desc);
            listaPatrimonio.addAll(porDesc);
        }
        // Por data de aquisição
        if(usarDateAq){
            porAquisicao = service.findItemPatrimonioByAquisicaoBetween(data_aquisicao_inicio, data_aquisicao_termino);
            listaPatrimonio.addAll(porAquisicao);
        }
        // Por data de baixa
        if(usarDateBx){
            porBaixa = service.findItemPatrimonioByBaixaBetween(data_baixa_inicio, data_baixa_termino);
            listaPatrimonio.addAll(porBaixa);
        }
        // Por encarregado
        if(encarregado != null){
            usarEncarregado = true;
            porEncarregado = service.findItemPatrimonioByEncarregado(encarregado);
            listaPatrimonio.addAll(porEncarregado);
        }
        // Para utilizarmos os filtros, utilizamos o método Collections.retainAll
        // Esse método apenas os elementos de a que estão em b
        // Assim, se aplicarmos sucessivamente, podemos filtrar e termos apenas a interseção de todas as listas
        if(usarDesc) listaPatrimonio.retainAll(porDesc);
        if(usarDateAq) listaPatrimonio.retainAll(porAquisicao);
        if(usarDateBx) listaPatrimonio.retainAll(porBaixa);
        if(usarEncarregado) listaPatrimonio.retainAll(porEncarregado);
        if(listaPatrimonio.isEmpty()) return manterPatrimonio(req, model);
        model.addAttribute("listaPatrimonio", listaPatrimonio);
        return "/patrimonio/manterPatrimonioView";        
    }
    
}
